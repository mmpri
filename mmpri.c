/* mmpri.c
 * Changes the message facility
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "rsyslog.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>
#include <syslog.h>
#include "conf.h"
#include "syslogd-types.h"
#include "srUtils.h"
#include "template.h"
#include "module-template.h"
#include "errmsg.h"
#include "parserif.h"

MODULE_TYPE_OUTPUT
MODULE_TYPE_NOKEEP
MODULE_CNFNAME("mmpri")

DEF_OMOD_STATIC_DATA

/* config variables */

/* define operation modes we have */
typedef struct _instanceData {
	int iFacility;
	int iSeverity;
} instanceData;

typedef struct wrkrInstanceData {
	instanceData *pData;
} wrkrInstanceData_t;

struct modConfData_s {
	rsconf_t *pConf;
};

static struct cnfparamdescr actpdescr[] = {
	{ "facility", eCmdHdlrGetWord, CNFPARAM_REQUIRED },
	{ "severity", eCmdHdlrGetWord, 0 }
};
static struct cnfparamblk actpblk = {
        CNFPARAMBLK_VERSION,
        sizeof(actpdescr)/sizeof(struct cnfparamdescr),
        actpdescr
};

static modConfData_t *loadModConf = NULL;/* modConf ptr to use for the current load process */
static modConfData_t *runModConf = NULL;/* modConf ptr to use for the current exec process */

BEGINbeginCnfLoad
CODESTARTbeginCnfLoad
	loadModConf = pModConf;
	pModConf->pConf = pConf;
ENDbeginCnfLoad

BEGINendCnfLoad
CODESTARTendCnfLoad
ENDendCnfLoad

BEGINcheckCnf
CODESTARTcheckCnf
ENDcheckCnf

BEGINactivateCnf
CODESTARTactivateCnf
	runModConf = pModConf;
ENDactivateCnf


BEGINfreeCnf
CODESTARTfreeCnf
ENDfreeCnf


BEGINcreateWrkrInstance
CODESTARTcreateWrkrInstance
ENDcreateWrkrInstance


BEGINisCompatibleWithFeature
CODESTARTisCompatibleWithFeature
ENDisCompatibleWithFeature


BEGINcreateInstance
CODESTARTcreateInstance
ENDcreateInstance


BEGINfreeInstance
CODESTARTfreeInstance
ENDfreeInstance

BEGINnewActInst
        struct cnfparamvals *pvals;
        int i;
CODESTARTnewActInst
        dbgprintf("newActInst (mmpri)\n");
        if ((pvals = nvlstGetParams(lst, &actpblk, NULL)) == NULL) {
                ABORT_FINALIZE(RS_RET_MISSING_CNFPARAMS);
        }

	CODE_STD_STRING_REQUESTnewActInst(1)
	CHKiRet(OMSRsetEntry(*ppOMSR, 0, NULL, OMSR_TPL_AS_MSG));
	CHKiRet(createInstance(&pData));
        pData->iFacility = LOG_INVLD;
        pData->iSeverity = LOG_INVLD;
        for (i = 0; i < actpblk.nParams; ++i) {
                if (!pvals[i].bUsed)
                        continue;
		if (!strcmp(actpblk.descr[i].name, "facility")) {
			uchar *fstr = (uchar*)es_str2cstr(pvals[i].val.d.estr, NULL);
			int n = decodeSyslogName(fstr, syslogFacNames);
			if (n < 0) {
				parser_errmsg("unknown facility name \"%s\"", fstr);
				free(fstr);
				ABORT_FINALIZE(RS_RET_ERR);
			}
			free(fstr);
			pData->iFacility = LOG_FAC(n);
		} else if (!strcmp(actpblk.descr[i].name, "severity")) {
			uchar *fstr = (uchar*)es_str2cstr(pvals[i].val.d.estr, NULL);
			int n = decodeSyslogName(fstr, syslogPriNames);
			if (n < 0) {
				parser_errmsg("unknown severity name \"%s\"", fstr);
				free(fstr);
				ABORT_FINALIZE(RS_RET_ERR);
			}
			free(fstr);
			pData->iSeverity = n;
		} else {
                        dbgprintf("mmpri: program error, non-handled param '%s'\n",
				  actpblk.descr[i].name);
                }
	}

CODE_STD_FINALIZERnewActInst
ENDnewActInst


BEGINfreeWrkrInstance
CODESTARTfreeWrkrInstance
ENDfreeWrkrInstance


BEGINdbgPrintInstInfo
CODESTARTdbgPrintInstInfo
ENDdbgPrintInstInfo


BEGINtryResume
CODESTARTtryResume
ENDtryResume


BEGINdoAction_NoStrings
	smsg_t **ppMsg = (smsg_t **) pMsgData;
	smsg_t *pMsg = ppMsg[0];
        instanceData *const pData = pWrkrData->pData;
CODESTARTdoAction
	pMsg->iFacility = pData->iFacility;
        if (pData->iSeverity != LOG_INVLD)
		pMsg->iSeverity = pData->iSeverity;
ENDdoAction


BEGINmodExit
CODESTARTmodExit
ENDmodExit


NO_LEGACY_CONF_parseSelectorAct

BEGINqueryEtryPt
CODESTARTqueryEtryPt
CODEqueryEtryPt_STD_OMOD_QUERIES
CODEqueryEtryPt_STD_OMOD8_QUERIES
CODEqueryEtryPt_STD_CONF2_QUERIES
CODEqueryEtryPt_STD_CONF2_OMOD_QUERIES
ENDqueryEtryPt



BEGINmodInit()
CODESTARTmodInit
        /* we only support the current interface specification */
	*ipIFVersProvided = CURR_MOD_IF_VERSION;
CODEmodInit_QueryRegCFSLineHdlr
	DBGPRINTF("mmpri: module compiled with rsyslog version %s.\n", VERSION);
ENDmodInit
