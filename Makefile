PROJECT=mmpri
SOURCES=mmpri.c
RSYSLOGDIR=
O=

# ####################
ifndef RSYSLOGDIR
  ifneq ($(MAKECMDGOALS),clean)
    .DEFAULT_GOAL=help
  endif
else
  ifeq ($(wildcard $(RSYSLOGDIR)/configure.ac),)
    $(error $(RSYSLOGDIR)/configure.ac not found)
  endif
  ifneq ($(wildcard $(RSYSLOGDIR)/config.h),)
    DEFINES=-DHAVE_CONFIG_H
  else
    VERSION=$(shell m4 xver.m4 $(RSYSLOGDIR)/configure.ac)
    # A temporary kludge. These defines are valid only for GNU/Linux.
    DEFINES=-DVERSION=\"$(VERSION)\" -DNDEBUG=1 -DOS_LINUX=1 -D_POSIX_PTHREAD_SEMANTICS=1
  endif
  ifneq ($(wildcard $(RSYSLOGDIR)/Makefile),)
    pkglibdir=$(shell $(MAKE) -f pkglibdir.mk RSYSLOGDIR="$(RSYSLOGDIR)")
  endif
endif

ifeq ($(MAKECMDGOALS),install)
  ifndef INSTALLDIR
    ifdef pkglibdir
      INSTALLDIR=$(pkglibdir)
    else
      $(error "Installation directory not defined. Please make sure rsyslog tree is configured or provide the INSTALLDIR variable")
    endif
  endif
endif

OBJECTS=$(SOURCES:.c=.o)
BUILD_LIBRARY=$(PROJECT).so
CPPFLAGS=-I$(RSYSLOGDIR) -I$(RSYSLOGDIR)/runtime -I$(RSYSLOGDIR)/grammar -I/usr/include/libfastjson $(DEFINES)
CFLAGS=-pthread -fPIC -DPIC $(O)
LDFLAGS=-shared  -fPIC -DPIC -Wl,-soname -Wl,$(BUILD_LIBRARY)
LIBS=
DISTFILES=$(SOURCES) Makefile pkglibdir.mk xver.m4 README

$(BUILD_LIBRARY): $(OBJECTS)
	$(CC) -o $(BUILD_LIBRARY) $(LDFLAGS) $(LIBS) $(OBJECTS)

install: $(BUILD_LIBRARY)
	install -d $(INSTALLDIR)
	install $(BUILD_LIBRARY) $(INSTALLDIR)/

clean:; -rm -f $(BUILD_LIBRARY) *.o

DISTDIR=$(PROJECT)

distdir:
	test -d $(DISTDIR) || mkdir $(DISTDIR)
	cp $(DISTFILES) $(DISTDIR)

dist: distdir
	tar zcf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

help:
	@echo Usage:
	@echo
	@echo make RSYSLOGDIR=DIR [O=OPTS]
	@echo "  Builds mmpri with rsyslog sources in DIR. It is recommended that"
	@echo "  rsyslog source tree be configured. If it is not, the build process"
	@echo "  will try to make some more or less educated guesses about rsyslog"
	@echo "  version, which are not guaranteed to succeed."
	@echo
	@echo "  Use the O= variable to pass additional options to the compiler, e.g."
	@echo "  O=-ggdb to enable debugging support."
	@echo
	@echo make [RSYSLOGDIR=DIR] [INSTALLDIR=IDIR]
	@echo "  Install the module."
	@echo "  INSTALLDIR supplies the name of the directory to install to."
	@echo "  If it is not defined, RSYSLOGDIR must be given (see above) and the"
	@echo "  rsyslog source tree it points to must be configured."
	@echo
	@echo make clean
	@echo "  Cleans up the source tree"

